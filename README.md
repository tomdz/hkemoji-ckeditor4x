# HKemoji for CKEditor 4x 
## This plugin is moved to [github](https://github.com/684102/hkemoji-ckeditor)

This plugin integrates the custom png/gif emoji and smiley for ckeditor 4 (with exists 4 example emoji package) . Similar with plugin emojione.
